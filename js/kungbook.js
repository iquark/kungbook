function Character(imageFile) 
{
   this.image = null;
   this.bitmap = null;
   this.init = function() {
         this.image = new Image();
         this.image.onload = handleImageLoad;
         this.image.onerror = handleImageError;
         this.image.src = 'img/'+imageFile+'.png';
         console.log("image: "+this.image);
         this.bitmap = new createjs.Bitmap(this.image);
         console.log("bitmap: "+this.bitmap);
      };
   this.getBitmap = function() {
         return bitmap;
      };
}

var Screen = {
	stage: null,
	currentName: "splash",
	drawScene: function() {
        console.log("Showing: "+this.currentName+" function: "+this[this.currentName]);
		return this[this.currentName]();
	},
	select: function(name) {
		this.currentName = name;
	},
	next: function() {
		switch(this.currentName) {
			case "splash":
				this.currentName = "selectCharacter";
				break;
			case "selectCharacter":
				this.currentName = "selectScenario";
				break;
			case "selectScenario":
				this.currentName = "selectMoves";
				break;
			case "selectMoves":
				this.currentName = "fight";
				break;
			case "fight":
				this.currentName = "ranking";
				break;
			default:
				this.currentName = "splash";
				break;
		}
        console.log("Current name: "+this.currentName);
        this.drawScene();
	},
	splash: function() {
        console.log("Splash!");
       var text = new createjs.Text("KungBook!!", "20px Arial bold", "#000");
       var circle = new createjs.Shape();
       
       var but = new Button();
       
       text.x = 100;
       text.textBaseline = "alphabetic"
       text.y = 100;
       this.stage.addChild(text);
       
	   circle.graphics.beginFill("#fff").drawCircle(0, 0, 50);
	   circle.x = 150;
	   circle.y = 200;
	   circle.onClick = function() { Screen.next(); console.log("clicked"); };
       this.stage.addChild(circle); 
	},
	selectCharacter: function() {
        console.log("Select character!");
       var text = new createjs.Text("Select Character!!", "20px Arial bold", "#000");
       var circle = new createjs.Shape();
       
       var but = new Button();
       
       text.x = 100;
       text.textBaseline = "alphabetic"
       text.y = 100;
       this.stage.addChild(text);
       
	   circle.graphics.beginFill("#fff").drawCircle(0, 0, 50);
	   circle.x = 150;
	   circle.y = 200;
	   circle.onClick = function() { Screen.next(); console.log("clicked"); };
       this.stage.addChild(circle); 
	},
	selectScenario: function() {
		
	},
	selectMoves: function() {
		
	},
	fight: function() {
       var character = new Character('character');
       var opponent = new Character('character1');
       var circle = new createjs.Shape();
       var circle2 = new createjs.Shape();

	   // load characters
	   character.init();
	   opponent.init();  
	   
		// load circle
	   circle.graphics.beginFill("#fff").drawCircle(0, 0, 50);
	   circle2.graphics.beginFill("#fff").drawCircle(0, 0, 50);
	   circle.x = 100;
	   circle.y = 100;
	   circle2.x = 500;
	   circle2.y = 100;
	   
	   circle.onClick = function() { moveLeft(character.bitmap) };
	   circle2.onClick = function() { moveRight(opponent.bitmap) };
	   
	   this.stage.addChild(circle);
	   this.stage.addChild(circle2);
	   
	   character.bitmap.x = 500;
	   character.bitmap.y = 200;
	   
	   opponent.bitmap.x = 100;
	   opponent.bitmap.y = 200;
	   
	   this.stage.addChild(character.bitmap);
	   this.stage.addChild(opponent.bitmap);
	},
	listOpponents: function() {
		
	},
	ranking: function() {
		
	},
};

var Actions = {
   attack: function(bitmap) {
      console.log("attack!");
      console.log("moving right: "+bitmap);
      createjs.Tween.get(bitmap) 
         .to({x:500},500, createjs.Ease.backInOut)
         .to({x:bitmap.x},300,createjs.Ease.bounceOut);
   
   },
   hit: function() {
      console.log("hit!");
   },
   miss: function() {
      console.log("miss!");
   }
}


function init() 
{
   var stage = new createjs.Stage("canvas");

   // set the stage for the screens
   Screen.stage = stage;
   
   Screen.drawScene();
	
   stage.update();
   
   createjs.Ticker.setFPS(30);
   createjs.Ticker.addListener(stage);
}


// handles
function handleImageLoad() 
{
   console.log("image loaded.");
}

function handleImageError() 
{
   console.log("error loading image.");
}

function moveRight(bitmap) {
      console.log("moving right: "+bitmap);
      createjs.Tween.get(bitmap) 
         .to({x:500},500, createjs.Ease.backInOut)
         .to({x:bitmap.x},300,createjs.Ease.bounceOut);
   }
   
function moveLeft(bitmap) {
      console.log("moving left: "+bitmap);
      createjs.Tween.get(bitmap) 
         .to({x:100},500, createjs.Ease.backInOut)
         .to({x:bitmap.x},300,createjs.Ease.bounceOut);
   };
