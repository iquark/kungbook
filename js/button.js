// from Codemasta92: https://gist.github.com/codemasta92

(function(k) {
var Button = function(a, x, y) {
this.initialize(a,x,y)
},p = Button.prototype = new createjs.DisplayObject;
 
p.left = 0;
p.top = 0;
p.ypos = 0;
p.image = null;
p.snapToPixel = true;
p.DisplayObject_initialize = p.initialize;
 
p.initialize = function(a,x,y) {
    this.DisplayObject_initialize();
    if (typeof a == "string") {
        this.image = new Image();
        this.image.src = a;
    } else {
        this.image = a;
    }
    this.top = y;
    this.left = x;
};
 
p.isVisible = function() {
return this.visible && this.alpha > 0 && this.scaleX != 0 && this.scaleY != 0 && this.image && (this.image.complete || this.image.getContext || this.image.readyState == 2)
};
 
p.DisplayObject_draw = p.draw;
p.draw = function(ctx, ignoreCache) {
if (this.DisplayObject_draw(ctx, ignoreCache))return true;
ctx.drawImage(this.image, 0,this.ypos, this.image.width, this.image.width, this.left, this.top, this.image.width, this.image.width);
return true
};
 
p.clone = function() {
var a = new Button(this.image, 0, 0);
this.cloneProps(a);
return a
};
 
p.toString = function() {
return"[Button (name=" + this.name + ")]"
};
 
p.onMouseOver = function(){
this.ypos = Math.floor(this.image.height/3);
}
p.onMouseOut = function() {
this.ypos = 0;
}
p.onClick = function(){
console.log("mousedown")
this.ypos = Math.floor(this.image.height/3*2);
}
 
k.Button = Button
})(window);
